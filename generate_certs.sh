#!/bin/sh

# ca
openssl req -new -x509 -nodes -days 365 -subj '/CN=ca.localhost' -keyout certs/ca.key -out certs/ca.crt

# server
openssl genrsa -out certs/server.key 2048
openssl req -new -key certs/server.key -subj '/CN=server.localhost' -out certs/server.csr
openssl x509 -req -extfile server_ext.cnf -in certs/server.csr -CA certs/ca.crt -CAkey certs/ca.key -CAcreateserial -days 365 -out certs/server.crt

# proxy
openssl genrsa -out certs/proxy.key 2048
openssl req -new -key certs/proxy.key -subj '/CN=proxy.localhost' -out certs/proxy.csr
openssl x509 -req -in certs/proxy.csr -CA certs/ca.crt -CAkey certs/ca.key -CAcreateserial -days 365 -out certs/proxy.crt

# client
openssl genrsa -out certs/client.key 2048
openssl req -new -key certs/client.key -subj '/CN=client.localhost' -out certs/client.csr
openssl x509 -req -in certs/client.csr -CA certs/ca.crt -CAkey certs/ca.key -CAcreateserial -days 365 -out certs/client.crt

# inspect
#openssl x509 --in certs/ca.crt -text --noout
#openssl x509 --in certs/server.crt -text --noout
#openssl x509 --in certs/proxy.crt -text --noout
#openssl x509 --in certs/client.crt -text --noout