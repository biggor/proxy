const fs = require('fs');
const https = require('https');

https
    .createServer(
        {
            // ...
            requestCert: true,
            rejectUnauthorized: true,
            ca: fs.readFileSync('certs/ca.crt'),
            cert: fs.readFileSync('certs/server.crt'),
            key: fs.readFileSync('certs/server.key')
            // ...
        },
        (req, res) => {
            console.log(
                "TLS Connection established successfully!",
                req.client.authorized ? "authorized" : "unauthorized",
                req.client.getProtocol(),
                req.client.getPeerCertificate().issuer.CN,
                req.client.getPeerCertificate().subject.CN
            );

            if (!req.client.authorized) {
                res.writeHead(401);
                return res.end('Invalid client certificate authentication.');
            }

            res.writeHead(200);
            res.end('Hello, world!\n');
        }
    )
    .listen(9443);