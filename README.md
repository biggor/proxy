# Traefik mTLS reverse proxy

Traefik reverse proxy configured to run locally on the client server (localhost). 

## certificates
generante certificates (ca, client, proxy(localhost), server):  
`./generate_certs.sh`

## server
`node server.js`

## proxy
`docker run --rm --name traefik -v $PWD:/etc/traefik --network host traefik`

## client
`node client.js`
or
`curl --cert certs/client.crt --key certs/client.key --cacert certs/ca.crt https://localhost:9443 --tlsv1.1 --tls-max 1.2`

## dockerized server and proxy
proxy listening 8080
server listen on 443

`docker compose up`

go to http:/localhost:8080 in a browser or
`curl localhost:8080`
or initiate the connection from the porxy instance
`docker exec -it proxy bash`
`apk add curl`
verify the mtls connection with the server
`curl --cert /etc/traefik/certs/client.crt --key /etc/traefik/certs/client.key --cacert /etc/traefik/certs/ca.crt https://server:443 --tlsv1.1 --tls-max 1.2`

check the connection through the local proxy
`curl localhost:8080`
